#!/usr/bin/env bash

samtools sort "$1" sorted | samtools view "$1" | awk '{print $6}' | awk '{if($1!="*"){print($1)}}' | awk '{gsub(/[0-9]+/," & ",$0); $1=$1}1' > temp1

sed 's/[^ ]//g' temp1 | awk '{i=(length*2)+1} {print i}' > temp2

awk 'FNR==NR { a[FNR""] = $0; next } { print a[FNR""], $0 }' temp2 temp1 > temp3

awk '{for(i=1;i<$1;i+=2) {for(x=1;x<=$(i+1);x++) {printf $(i+2) " "}} {print "\n"}}' temp3 | awk '{if($1!="") {print $0}}' > temp4

sed 's/[^ ]//g' temp4 | awk '{i=(length*2)+1} {print ((i-1)/2)}' > temp5

awk 'FNR==NR { a[FNR""] = $0; next } { print a[FNR""], $0 }' temp5 temp4 > temp6

rm temp1

rm temp2

rm temp3

rm temp4

awk '{for(i=1;i<=$1;i++) {if($(i+1) != "M") {printf "1 "} else {printf "0 "}}{print "\n"}}' temp6 | awk '{if($1!="") {print $0}}' > temp7

awk 'FNR==NR { a[FNR""] = $0; next } { print a[FNR""], $0 }' temp5 temp7 > temp8

awk '{if(y<$1) y=$1} {x=NR}  END{for(i=1;i<=x;i++)print y}' temp8 > max

awk 'FNR==NR { a[FNR""] = $0; next } { print a[FNR""], $0 }' max temp8 > tempA

awk '{y=$1} {for(i=y-$2;i!=0;i--){printf "X "}} {for(i=1;i<$2;i++){printf $(2+i) " "}} {print "\n"}' tempA | awk '{if($1!="") {print $0}}' > tempB

awk 'FNR==NR { a[FNR""] = $0; next } { print a[FNR""], $0 }' max tempB > tempC

awk '{x=$1} {if(x > y) y = $1}{for(i=1;i<=$1;i++) A[i]+=1} END{for(i=y;i!=0;i--){print A[i]}}' temp8 > num

awk '{y=$1} {for(i=1;i<=$1;i++){if($(i+1)=="X"){array[i]+=0}else{array[i]+=$(i+1)}}} END{for(i=1;i<=y;i++){print array[i]}}' tempC > tempD

awk 'FNR==NR { a[FNR""] = $0; next } { print a[FNR""], $0 }' tempD num > tempE

awk '{print w=1+s++ " " $1/$2}' tempE > Data.tsv

rm temp5

rm temp6

rm temp7

rm temp8

rm tempA

rm tempB

rm tempC

rm tempD

rm tempE

rm num

rm max

Rscript Plot.R

rm Data.tsv

rm sorted.bam