#!/usr/bin/env bash

samtools sort $1 sorted

# USER PLEASE NOTE:
# IN ORDER TO RUN THIS ON YOUR OWN BAM FILE PLEASE CHANGE THE REFERENCE
# LOCATED BELLOW SUCH THAT "Trial.fsa" WILL BE REPLACED BY YOUR REFERENCE
# THIS IS TEMPORARY AND WILL SOON BE CHANGED SO YOU CAN EASILY INPUT BOTH
# YOU BAM FILE AND ITS REFERENCE RIGHT AFTER MO.SH 

samtools fillmd -e sorted.bam Trial.fsa > mdfile

cat mdfile | awk '{if($10!=""){print $10}}' | sed 's/.\{1\}/& /g' > temp1

sed 's/[^ ]//g' temp1 | awk '{i=(length)} {print i}' > temp2

awk '{if(y<$1) y=$1} {x=NR}  END{for(i=1;i<=x;i++)print y}' temp2 > max

awk 'FNR==NR { a[FNR""] = $0; next } { print a[FNR""], $0 }' temp2 temp1 > temp3

awk 'FNR==NR { a[FNR""] = $0; next } { print a[FNR""], $0 }' max temp3 > tempA

awk '{y=$1} {for(i=y-$2;i!=0;i--){printf "X "}} {for(i=1;i<$2+1;i++){printf $(2+i) " "}} {print "\n"}' tempA | awk '{if($1!="") {print $0}}' >tempB

awk 'FNR==NR { a[FNR""] = $0; next } { print a[FNR""], $0 }' max tempB > tempC

awk '{x=$1} {if(x > y) y = $1}{for(i=1;i<=$1;i++) A[i]+=1} END{for(i=y;i!=0;i--){print A[i]}}' temp3 > num

awk '{y=$1} {for(i=1;i<=$1;i++){if($(i+1)=="X"||$(i+1)=="="){array[i]+=0}else{array[i]+=1}}} END{for(i=1;i<=y;i++){print array[i]}}' tempC > tempD

awk 'FNR==NR { a[FNR""] = $0; next } { print a[FNR""], $0 }' tempD num > tempE

awk '{print w=1+s++ " " $1/$2}' tempE > Data.tsv

rm mdfile

rm temp1

rm temp2

rm temp3

rm max

rm tempA

rm tempB

rm tempC

rm num

rm tempD

rm tempE

rm sorted.bam

Rscript Plot2.R

rm Data.tsv
